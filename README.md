# RUN SERVER

* python -m venv .venv
* .\.venv\Scripts\Activate.ps1
* pip install -r requirements.txt
* python .\manage.py runserver

In your browser, go to http://127.0.0.1:8000/