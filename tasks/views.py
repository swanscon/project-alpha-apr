from django.urls import reverse_lazy, reverse
from tasks.models import Task
from django.views.generic import CreateView, ListView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(self):
        return reverse("show_project", args=[3])
        # Could not figure out how to get primary key of the Project
        # instance for the newly created task, so hardcoded to a Project
        # with a known primary key


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    context_object_name = "tasks"
    template_name = "tasks/mine.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "tasks/mine.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
